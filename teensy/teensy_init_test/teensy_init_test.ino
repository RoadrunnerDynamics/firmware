
int ledPin = 13;
void setup() {
  // initialize digital pin for Teensy 4.1 as an output.
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); // USB is always 12 or 480 Mbit/sec
  Serial.println("Teensy 4.1 ready...");
}

void loop() {
  int incomingByte;
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    char character = incomingByte;
    Serial.print("USB Serial received: ");
    Serial.println(incomingByte, DEC);
    
    if(character == '0') {
      Serial.println(character);
      digitalWrite(ledPin, LOW);
      delay(1000);  
    }
    if(character == '1') {
      Serial.println(character);
      digitalWrite(ledPin, HIGH);
      delay(1000);  
    }
  }
}