# Teensy 4.1

## Getting started
- Read https://www.pjrc.com/teensy/loader.html
- Load [teensy_init_test](teensy_init_test)

## Docs
- https://www.pjrc.com/store/teensy41.html
- https://forum.pjrc.com/index.php?threads/teensy-4-1-where-do-i-connect-to-use-third-spi.66144/#post-268934
- https://www.pjrc.com/teensy/td_uart.html

## PINS
![alt text](screenshots/T4.1-Cardlike.jpg)
![alt text](screenshots/T4.1-mux.jpg)